import functions_framework
from google.cloud import storage


def upload_blob(bucket_name, source_file_name, destination_blob_name):
  """Uploads a file to the bucket."""
  storage_client = storage.Client()
  bucket = storage_client.get_bucket(bucket_name)
  blob = bucket.blob(destination_blob_name)

  blob.upload_from_filename(source_file_name)

  print('File {} uploaded to {}.'.format(
      source_file_name,
      destination_blob_name))


# Triggered by a change in a storage bucket
@functions_framework.cloud_event
def start_cleansing(cloud_event):
    data = cloud_event["data"]

    bucket = data["bucket"]
    name = data["name"]
    metageneration = data["metageneration"]
    timeCreated = data["timeCreated"]
    updated = data["updated"]

    print(f"Bucket: {bucket}")
    print(f"File: {name}")
    
    # TODO
    # read file from GCS into dataframe
    
    
    # TODO
    # cleansing dataframe
    
    # TODO
    # load data into bq
    # 1. load to GCS
    # 2. load from gcs to bq
