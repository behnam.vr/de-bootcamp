import functions_framework
from google.cloud import storage
import pandas as pd
from datetime import datetime
from google.cloud import bigquery


# Cloud Function to be triggered by GCS events
@functions_framework.cloud_event
def start_cleansing(cloud_event):
    data = cloud_event["data"]
    bucket_name = data["bucket"]
    file_name = data["name"]
    print(f"Bucket: {bucket_name}")
    print(f"File: {file_name}")

    #READ FROM GCS
    df = read_file_from_gcs(bucket_name, file_name)

    # CLEANING DATA
    df_cleaned = cleanse_data(df)

    # LOAD TO GCS
    destination_blob_name = f"cleansed/{file_name}"
    save_cleansed_df_to_gcs(df_cleaned, bucket_name, destination_blob_name)

    # LOAD FROM GCS TO BQ
    project_id = "trial-5-382407" 
    dataset_id = "saman_temp"
    table_name = "cleaned_data"  
    load_data_to_bigquery(project_id, dataset_id, table_name, bucket_name, destination_blob_name)

# FUNCTIONS
def read_file_from_gcs(bucket_name, file_name):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(file_name)
    columns = ["date", "city", "customer_name", "orders", "total_price", "payment_method", "card_number"]
    df = pd.read_csv(blob.open(), names=columns)
    return df


def cleanse_data(df):
    
    def date_str_to_date_object(date_str):
        return datetime.strptime(date_str, '%d/%m/%Y %H:%M')

    df["date"] = df["date"].apply(date_str_to_date_object)
    df["order"] = df["orders"].apply(lambda orders_txt: orders_txt.split(","))
    df = df.explode("order")
    df["order"] = df["order"].apply(lambda o: o.strip())
    df["product_price"] = df["order"].apply(lambda o: float(o.split("-")[-1].strip()))


    def name_extractor(order):
        name_and_size = order.split("-")[0]
        without_size = name_and_size.split()
        name = " ".join(without_size[1:])
        return name
    
    df["product_name"] = df["order"].apply(name_extractor)
    df["prodduct_size"] = df["order"].apply(lambda o: o.split(' ')[0])


    def category_extractor(order_txt):
        order_list = order_txt.split("-")
        if len(order_list) == 3:
            return order_list[1].strip()


    df["order_category"] = df["order"].apply(category_extractor)

    return df


def save_cleansed_df_to_gcs(df, bucket_name, destination_blob_name):
    blob = storage.Client().bucket(bucket_name).blob(destination_blob_name)
    blob.upload_from_string(df.to_csv(index=False), 'text/csv')


def load_data_to_bigquery(project_id, dataset_id, table_name, bucket_name, blob_name):
    client = bigquery.Client(project=project_id)
    dataset_ref = client.dataset(dataset_id)
    table_ref = dataset_ref.table(table_name)
    job_config = bigquery.LoadJobConfig(source_format=bigquery.SourceFormat.CSV, skip_leading_rows=1)
    uri = f"gs://{bucket_name}/{blob_name}"
    job = client.load_table_from_uri(uri, table_ref, job_config=job_config)
    job.result()

    print(f"Loaded {job.output_rows} rows into {project_id}:{dataset_id}.{table_name}.")
