from main import start_cleansing


test_event = {
    "attributes": {
        "specversion": "1.0",
        "id": "7915486136456811",
        "source": "//storage.googleapis.com/projects/_/buckets/2023-de-final-project-team-1",
        "type": "google.cloud.storage.object.v1.finalized",
        "datacontenttype": "application/json",
        "subject": "objects/2023_3_31_birmingham_31-03-2023_09-00-00.csv",
        "time": "2023-07-12T07:49:59.192557Z",
        "bucket": "2023-de-final-project-team-1",
    },
    "data": {
        "kind": "storage#object",
        "id": "2023-de-final-project-team-1/2023_3_31_birmingham_31-03-2023_09-00-00.csv/1689148199172806",
        "selfLink": "https://www.googleapis.com/storage/v1/b/2023-de-final-project-team-1/o/2023_3_31_birmingham_31-03-2023_09-00-00.csv",
        "name": "2023_3_31_birmingham_31-03-2023_09-00-00.csv",
        "bucket": "2023-de-final-project-team-1",
        "generation": "1689148199172806",
        "metageneration": "1",
        "contentType": "text/csv",
        "timeCreated": "2023-07-12T07:49:59.192Z",
        "updated": "2023-07-12T07:49:59.192Z",
        "storageClass": "STANDARD",
        "timeStorageClassUpdated": "2023-07-12T07:49:59.192Z",
        "size": "40997",
        "md5Hash": "EYFepfVLWtsPJKL5o+Dt5w==",
        "mediaLink": "https://storage.googleapis.com/download/storage/v1/b/2023-de-final-project-team-1/o/2023_3_31_birmingham_31-03-2023_09-00-00.csv?generation=1689148199172806&alt=media",
        "crc32c": "gOIbEw==",
        "etag": "CMb1ibPXiIADEAE=",
    },
}

start_cleansing(test_event)
